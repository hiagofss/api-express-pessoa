const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()

mongoose.connect('mongodb://pessoa:pessoa@ds237770.mlab.com:37770/projeto_pessoa')

// configura app para usar bodyParser()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/pessoa', require('./routes/pessoaRoute'))

module.exports = app