const Pessoa = require('../models/pessoa')

module.exports = {
  create: (req, res, next) => {
    let pessoa = new Pessoa(req.body)

    pessoa.save()
      .then(saved => res.json(saved))
      .catch(e => next(e))
    
  },

  find: (req, res, next) => {
    Pessoa.findById(req.params.id)
      .then(user => res.json(user))
      .catch(e => next(e))
  },

  findAll: (req, res) => {
    Pessoa.find()
      .then(users => res.json(users))
      .catch(e => next(e))
  },

  update: (req, res, next) => {
    Pessoa.findById(req.params.id)
      .then( found => {
        found.set(req.body)
        found.save()
          .then(saved => res.json(saved))
          .catch(e => next(e))
      })
      .catch(e => next(e))
  },

  delete: (req, res) => {
    Pessoa.findByIdAndRemove(req.params.id)
      .then(deleted => {
        res.json({
          message: 'Pessoa deleted',
          id: deleted._id
        })
      })
      .catch(e => next(e))
  }
}