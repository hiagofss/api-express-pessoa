const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var PessoaSchema   = new Schema({
    nome: String,
    idade: Number,
    sexo: String,
});

module.exports = mongoose.model('Pessoa', PessoaSchema);